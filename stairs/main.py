with open('real.txt') as f:
    x = int(f.readline().split(' ')[1])
    y = int(f.readline().split(' ')[1])
    lines = f.readlines()
    lines = [x.split(' ') for x in lines]
    lines = [[int(y) for y in x] for x in lines]

shortestPath = [[None for i in range(y)] for j in range(x)]

print(sum([max(x) for x in lines]))


def findBestSolution(i, j, jumpLeft):
    if i < 0 or j < 0 or j == y or i == x:
        return 0

    if shortestPath[i][j] is not None:
        return shortestPath[i][j]

    toTest = {(i - 1, j + 1, jumpLeft), (i, j + 1, jumpLeft), (i + 1, j + 1, jumpLeft)}

    if jumpLeft > 0:
        for it in range(x):
            toTest.add((it, j + 1, jumpLeft - 1))

    shortestPath[i][j] = lines[j][i] + max([findBestSolution(t[0], t[1], t[2]) for t in toTest])

    return shortestPath[i][j]


results = []
for t in range(x):
    findBestSolution(t, 0, 1)
    results.append(max([0 if shortestPath[i][0] is None else shortestPath[i][0] for i in range(x)]))
    shortestPath = [[None for i in range(y)] for j in range(x)]

print(max(results))
