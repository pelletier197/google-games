string = "smmaaggemgegmseeemaaemgmaaemaasasesmgmegaeaagmmegssggaammmaagseeamemeaeaaegsmseemmsesagaggeaagmgagma"


def get_all_substrings(input_string):
    length = len(input_string)
    return [input_string[i:j + 1] for i in xrange(length) for j in xrange(i, length)]


def isPalyndrme(n):
    return str(n) == str(n)[::-1]


substrings = get_all_substrings(string)
choices = set(list(string))

longest = ""
for i in range(len(string)):
    for pal in substrings:
        if len(pal) > len(longest):
            for j in range(len(pal)):
                for k in choices:
                    if isPalyndrme(pal[0:j]+k+pal[j+1:]):
                        longest = ''.join(pal[0:j]+k+pal[j+1:])
                        print(longest)

