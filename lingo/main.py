import itertools
from operator import mul


def sum_digits(n):
    s = 0
    while n:
        s += n % 10
        n //= 10
    return s

def sums_full_digits(x):
    return sum([sum_digits(y) for y in x])

# read lines
lines = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107,
         109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179]

perfectCubes = {1, 8, 27, 64, 125, 216, 343, 512, 729, 1000, 1331, 1728, 2197, 2744, 3375}

for subset in itertools.combinations(lines, 5):
    if sum(subset) == 540 and sums_full_digits(subset) in perfectCubes:
        print(subset)
        print(reduce(mul, subset, 1))
        break

