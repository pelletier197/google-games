from operator import mul
from functools import reduce

solutions = []


def recursiteTest(questionsLeft, chain, timesUsedGoogler):
    if questionsLeft == 0:
        solutions.append(chain)
        return
    test1 = chain[:]
    test1.append({'action': 'none', 'prob': 0.25})
    recursiteTest(questionsLeft - 1, test1, timesUsedGoogler)

    if (len(chain) == 0 or chain[len(chain) - 1]['action'] == 'none') and questionsLeft != 10 and questionsLeft != 1:
        test2 = chain[:]
        test2.append({'action': 'power', 'prob': 0.5})
        recursiteTest(questionsLeft - 1, test2, timesUsedGoogler)

    if len(chain) == 0 or chain[len(chain) - 1]['action'] != 'ask':
        test3 = chain[:]
        test3.append({'action': 'ask', 'prob': questionsLeft * 0.1})
        recursiteTest(questionsLeft - 1, test3, timesUsedGoogler)

        test4 = chain[:]
        test4.append({'action': 'ask', 'prob': ((1 - (questionsLeft * 0.1)) / 3)})
        recursiteTest(questionsLeft - 1, test4, timesUsedGoogler)

    if timesUsedGoogler != 2:
        test4 = chain[:]
        test4.append({'action': 'google', 'prob': 0.9})
        recursiteTest(questionsLeft - 1, test4, timesUsedGoogler + 1)

        test5 = chain[:]
        test5.append({'action': 'google', 'prob': 0.33})
        recursiteTest(questionsLeft - 1, test5, timesUsedGoogler + 1)


recursiteTest(10, [], 0)

print(max([reduce(mul, [y['prob'] for y in x], 1) for x in solutions]))
